package com.wsm.sys;

import java.util.Scanner;
/**
 * System 系统类的学习...
 */
public class SysDemo {
    //main 主方法...
    public static void main(String[] args) {

        /** 成员属性: in out err */
            //in:   配合Scanner 扫描器,可以实现控制台的输入流...
            //out:  这个,最常用: System.out.print(); 返回一个输出流,输出信息
            //err:  和out 类似,但是它输出的信息一般表示错误,比较显眼的 红色信息！
        //创建一个扫描仪
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入一段话s1:");
        String s1 = scanner.next();
        System.out.println("请输入一段话s2:");
        String s2 = scanner.next();
        System.err.println("s1:"+s1);
        System.err.println("s2:"+s2);
        //注意: 不要同时两个
        // scanner.nextxxx() 写在一起,这些方法执行时都会造成堵塞，等待用户在命令行输入数据回车确认.
        // 写在一起,输入了一个回车,则所有的 nextxxx() 都结束了！
        scanner.close();                /** 要注意关闭流o 不然占用程序资源... */

        /** currentTimeMillis */
        long timeStamp = System.currentTimeMillis();    //返回当前时间,时间戳;
        //时间戳: 值当前日期距离,格林威治时间(GMT)1970-01-01  的毫秒数~
        //使用:
        //  通常可以,用于生产一个时间戳, 时间唯一的一个ID, 用作在数据库/文件名...表示数据唯一~
        System.out.println(timeStamp);


        /** getProperty(): 获取系统的参数信息！*/
        String javaVersion = System.getProperty("java.version");
        System.out.println("java的环境版本:" + javaVersion);

        String javaHome = System.getProperty("java.home");
        System.out.println("java的安装路径:" + javaHome);

        String osName = System.getProperty("os.name");
        System.out.println("操作系统的名称:" + osName);

        String osVersion = System.getProperty("os.version");
        System.out.println("操作系统的版本:" + osVersion);

        String userName = System.getProperty("user.name");
        System.out.println("计算机用户账户名:" + userName);

        String userHome = System.getProperty("user.home");
        System.out.println("用户的主目录:" + userHome);

        String userDir = System.getProperty("user.dir");
        System.out.println("用户当前工作目录:" + userDir);
    }
}
