package com.wsm.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/** 日历类对象Calendar 使用； */
public class CalendarDemo {
    public static void main(String[] args) {
        //1.实例化
        //方式一：创建其子类（GregorianCalendar）的对象
//        Calendar gregoriancalendar = new GregorianCalendar();

        //方式二：调用其静态方法getInstance()
        Calendar calendar = Calendar.getInstance();                 //获取当前日期的: Calendar日历对象！
//        System.out.println(calendar.getClass());

        //2.常用方法
        //get():
        //  根据常量参数,获取对应的日期值~
        int days = calendar.get(Calendar.DAY_OF_MONTH);             //获取当月的第几天~
        System.out.println("当月的第"+days+"天");                                   //打印！
        System.out.println("当年的第"+calendar.get(Calendar.DAY_OF_YEAR)+"天");     //获取当年的第几天~
//        ChronoField.ALIGNED_WEEK_OF_MONTH
        System.out.println("当月的第"+calendar.get(Calendar.WEEK_OF_MONTH)+"周");   //获取当月的第几周~
        
        //set()
        //calendar可变性
        //  给当前日历对象 进行年月赋值 返回值void: 直接返回给当前对象赋值~
        calendar.set(Calendar.DAY_OF_MONTH,22);                     //给当前日历对象设置为: 当月第22天
        days = calendar.get(Calendar.DAY_OF_MONTH);
        System.out.println("修改日历为当月,第"+days+"天");                                   //打印: 22

        //add(): 给当前日历对象进行 + - 操作
        calendar.add(Calendar.DAY_OF_MONTH,-3);
        days = calendar.get(Calendar.DAY_OF_MONTH);
        System.out.println(days);                                   //打印: 22-3

        //getTime():日历类---> Date
        //  根据当前日历对象的时间,返回一个Date类型数据~
        Date date = calendar.getTime();
        System.out.println(date);

        //setTime():Date ---> 日历类
        //  将一个Date类型数据,转换为: 日历对象！
        Date date1 = new Date();
        calendar.setTime(date1);
        days = calendar.get(Calendar.WEEK_OF_MONTH);
        System.out.println("当前第"+days+"周");
    }
}
