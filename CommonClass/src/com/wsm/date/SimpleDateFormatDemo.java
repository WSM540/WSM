package com.wsm.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * SimpleDateFormat类:
 *      Date类的格式化 —— 解析
 *      格式化：日期 --->字符串
 *      解析：格式化的逆过程，字符串 ---> 日期
 *
 */
public class SimpleDateFormatDemo {
    public static void main(String[] args) throws ParseException {
        //实例化SimpleDateFormat:使用默认的构造器
        SimpleDateFormat sdf = new SimpleDateFormat();  /** 默认的解析规则就是：(两位)年 月 日 ... */

        //格式化：日期 --->字符串
        Date date = new Date();
        System.out.println(date);

        //格式化~
        String format = sdf.format(date);
        System.out.println(format);

        //解析：格式化的逆过程，字符串 ---> 日期
        Date date1 = sdf.parse(format);             /** 需要处理异常: 这里就throws 抛出了~ */
        System.out.println(date1);

        /** 使用最多... */
        //*************按照指定的方式格式化和解析：调用带参的构造器*****************
//        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyy.MMMMM.dd GGG hh:mm aaa");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        //格式化
        String format1 = sdf1.format(date);
        System.out.println(format1);
        //解析:要求字符串必须是符合SimpleDateFormat识别的格式(通过构造器参数体现),
        //否则，抛异常~
        Date date2 = sdf1.parse("2020-02-18 11:48:27");
        System.out.println(date2);
    }
}
