package com.wsm.date;

import java.util.Date;

/**
 * java.sql.Date对应着数据库中的日期类型的变量
 *      java.sql.Date extends java.util.Date  继承Util.Date
 */
public class SqlDateDemo {

    public static void main(String[] args) {
        //创建java.sql.Date对象:
        //对应数据中的日期变量！`虽然如此但是好像还是很少使用,一般也直接使用 util.Date`
        java.sql.Date date = new java.sql.Date(35235325345L);  //它只包含日期而没有时间部分,输出时候只显示年月日！
        System.out.println(date);                              //1971-02-13

        /** 如何将java.util.Date对象转换为java.sql.Date对象 */
        //情况一:
        //sql.Date 本质上继承了 util.Date
        Date date1 = new java.sql.Date(2343243242323L);         //子类赋值父类
        java.sql.Date dateS = (java.sql.Date) date1;            //然后在强转回, 因为date1 本来就是 sql.Date类型 可以强转回 sql.Date;
        System.out.println(dateS);


        /** 运行报错！ 父类前置转换成子类... */
        //情况二:
//        java.sql.Date date2 = (java.sql.Date)new Date();        //父类直接强转子类,编译不报错运行报错
        //因为:
        //      子类直接向上转换成父类, 并可以强转回子类:
        //          子类本身继承父类拥有父类的属性方法,本身属于 父类/子类可以来回转换！
        //      但:
        //          子类继承父类,可以声明自己特有的方法属性... 父类直接转换子类,可能回不存在特有的属性方法(); 故程序报错！


        /** java.util.Date对象转换为java.sql.Date对象 */
        //情况三:
        //      util.Date 存在一个方法 getTime(); 返回,当前对象时间戳。
        //而:   sql.Date  刚好构造函数, 可以通过..时间戳声明！
        Date date3 = new Date();
        java.sql.Date date4 = new java.sql.Date(date3.getTime());
        System.out.println("Java.util.Date:"+date3);
        System.out.println("Java.sql.Date:"+date4);
    }
}
