package com.wsm.date;

import org.junit.Test;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * JDK8之后的日期API
 *      LocalDate       年月日~
 *      LocalTime       时分秒~
 *      LocalDateTime   年月日时分秒~
 *      三者,方法类似,都是JDK8 之后新增的日期类,分别用于表示不同的时间 日期 时间日期 描述的Java对象！
 */
public class LocalDateTimeDemo {

    /** LocalDate LocalTime LocalDateTime 使用/常用方法();
     *      LocalDateTime相较于LocalDate、LocalTime，使用频率要高
     *      类似于Calendar
     * */
    public static void main(String[] args) {
        //now():获取当前的日期、时间、日期+时间
        LocalDate localDate = LocalDate.now();
        LocalTime localTime = LocalTime.now();
        LocalDateTime localDateTime = LocalDateTime.now();

        System.out.println("localDate:\t"+localDate);
        System.out.println("localTime:\t"+localTime);
        System.out.println("localDateTime:\t"+localDateTime);
        /** 三个类重写了toString(); 直接输入的就是中国的日期格式~ */


        //of():设置指定的年、月、日、时、分、秒。没有偏移量
        LocalDateTime localDateTime1 = LocalDateTime.of(2020, 10, 6, 13, 23, 43);
        System.out.println(localDateTime1);


        //getXxx()：获取相关的属性
        System.out.println("获取当前日是这个月的第几天"+localDateTime.getDayOfMonth());      //获取当前日是这个月的第几天
        System.out.println("获取当前时间是一周中的哪一天"+localDateTime.getDayOfWeek());       //获取当前时间是一周中的哪一天
        System.out.println("获取当前时间是一周中的哪一天"+localDateTime.getMonth());           //获取当前月份
        System.out.println("获取当前月份"+localDateTime.getMonthValue());      //获取月份信息
        System.out.println("获取分钟"+localDateTime.getMinute());          //获取分钟
        /** 三个类均可调用类似的方法~ 注意要存在符合的条件~ */

        /** 体现不可变性 */
        //withXxx():设置相关的属性
        //  LocalDate　对象通过withxx(); 给日期对象设置日期属性,但是,并不会改变当前对象,而是会返回一个对象,是更高后的日期~ 本类并不会收受到影响~
        LocalDate localDate1 = localDate.withDayOfMonth(22);
        System.out.println("更新前对象:"+localDate);
        System.out.println("更新后对象:"+localDate1);

        //plusxxx();    向当前对象添加几天、几周、几个月、几年、几小时
        LocalDateTime localDateTime3 = localDateTime.plusMonths(3);
        System.out.println(localDateTime);
        System.out.println(localDateTime3);

        System.out.println("-------------------------------");

        //minusxxx();   向当前对象减去几天、几周、几个月、几年、几小时
        LocalDateTime localDateTime4 = localDateTime.minusMonths(6);
        System.out.println(localDateTime);
        System.out.println(localDateTime4);

        //.format();
        //打印format(); 数据
        System.out.println(localDateTime.format(DateTimeFormatter.ofPattern("yyyyMMdd HH:ss:mm")));

    }

    //JDK8之前日期问题...Date存在便宜量~
    @Test
    public void offsetDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        //Date(year, month, date);
        Date d1 = new Date(2000, 9, 9);     //创建一个2020 9月9日的日期对象;
        System.out.println("Date类存在偏移量:");
        System.out.println("未设置偏移量输出:"+sdf.format(d1));

        //设置便宜量:
        //Date 正因为存在偏移量,而被淘汰！ 而年便宜了 1900年 月默认从0开始~所以 2020 09 09  应该这个样写！
        Date d2 = new Date(2000-1900, 9-1, 9);
        System.out.println("设置偏移量输出: "+sdf.format(d2));   //正常输出 20200909
    }
}
