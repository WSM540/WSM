package com.wsm.date;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

/***
 * Instant 类的Demo学习
 *
 */
public class InsantDemo {
    public static void main(String[] args) {
        //now():获取本初子午线对应的标准时间
        Instant instant = Instant.now();
        System.out.println(instant);//2019-02-18T07:29:41.719Z      //

        //北京世界东八区！ 1949年起的逾半世纪，在中国大陆、台湾、香港、澳门所使用的标准时间皆为东八区（UTC+8）时间
        //中国,占五个时区.分别是东5区 东6区 东7区 东8区 东9区 据北京较远的地区也有不使用北京时间的 比如乌鲁木齐时间就是东九区时间,比北京时间早一个小时 世界分24个时区

        //添加时间的偏移量
        OffsetDateTime offsetDateTime = instant.atOffset(ZoneOffset.ofHours(8));
        System.out.println(offsetDateTime);//2019-02-18T15:32:50.611+08:00

        //toEpochMilli(): 获取自1970年1月1日0时0分0秒（UTC）开始的毫秒数  ---> Date类的getTime()
        long milli = instant.toEpochMilli();
        System.out.println(milli);

        //ofEpochMilli():通过给定的毫秒数，获取Instant实例  -->Date(long millis)
        Instant instant1 = Instant.ofEpochMilli(1550475314878L);
        System.out.println(instant1);
    }
}
