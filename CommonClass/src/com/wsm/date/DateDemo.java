package com.wsm.date;
import java.util.Date;

/**
 * Java.util.Date 时间类的使用:
 */
public class DateDemo {
    public static void main(String[] args) {
        //Java.util 包提供了 Date类来封装当前的日期和时间
        //Date类提供两个构造函数来实例化 Date 对象

        //new Date(); 构造器;
        Date d1 = new Date();           //默认获取当前日期;
        System.out.println("new Date():\t"+d1);         //Date类默认重写的 toString(); 不直接打印地址！但,因为Java是老外开发的,日期格式并不符号中国~

        //new Date(Long l); 构造器;
        long l = System.currentTimeMillis();    //获取当前日期时间戳..毫秒数~
        Date d2 = new Date(l);          //根据: 给定的时间戳,获取指定日期...
        System.out.println("new Date(Long):\t"+d2);

        //还有很多其它的构造器,但是都过期了..
        //Java注解: @Deprecated 注释的方法类,表示JDK以过期不在维护的类,但并不影响使用...!

        /**
         * Date常用方法：
         */
        System.out.println("getTime():\t"+d1.getTime());       //getTime():  返回自1970 年 1 月 1 日 00:00:00 GMT 以来此Date对象,表示的毫秒数;

        System.out.println("toString():\t"+d1.toString());      //toString(): 把此 Date 对象转换为以下形式的 String： dow mon dd hh:mm:ss zzz yyyy 格式;
        // dow 是一周中的某一天 (Sun, Mon, Tue,  Wed, Thu, Fri, Sat)
        // zzz是时间标准,

        /** 其它很多方法都过时了 */
    }
}

