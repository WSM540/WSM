package com.wsm.mathDemo;

import org.junit.Test;

/**
 * Math 数学类的使用Demo
 */
public class MathDemo {

    public static void main(String[] args) {
    /** 算术计算 */
        System.out.println("--------算术计算-----------");
        System.out.println(Math.sqrt(16));          // 4.0      4^2 4的二次方=16
        System.out.println(Math.cbrt(8));           // 2.0      2^3 2的三次方= 8
        System.out.println(Math.pow(3, 2));         // 9.0      3^2 3的二次方=16     几次方就是, x乘x次! 3x3=9
        System.out.println(Math.max(2.3, 4.5));     // 4.5      判断最大值！
        System.out.println(Math.min(2.3, 4.5));     // 2.3      判断最小值！

        /**
         * abs求绝对值: 就是把一个数变成 + 正数！
         */
        System.out.println(Math.abs(-10.4));        // 10.4
        System.out.println(Math.abs(10.1));         // 10.1

    /** 进位 */
        System.out.println("--------进位-----------");
        /**
         * ceil天花板的意思，就是逢余进一
         *      正数+1
         *      小数+1, 小数+1~
         */
        System.out.println("--------ceil-----------");
        System.out.println(Math.ceil(-10.1));       // -10.0
        System.out.println(Math.ceil(10.7));        // 11.0
        System.out.println(Math.ceil(-0.7));        // -0.0
        System.out.println(Math.ceil(0.0));         // 0.0
        System.out.println(Math.ceil(-0.0));        // -0.0
        System.out.println(Math.ceil(-1.7));        // -1.0

        /**
         * floor地板的意思，就是逢余舍一
         */
        System.out.println("--------floor-----------");
        System.out.println(Math.floor(-10.1));      // -11.0
        System.out.println(Math.floor(10.7));       // 10.0
        System.out.println(Math.floor(-0.7));       // -1.0
        System.out.println(Math.floor(0.0));        // 0.0
        System.out.println(Math.floor(-0.0));       // -0.0

        /**
         * rint 四舍五入，返回double值 注意.5的时候会取偶数 异常的尴尬=。=
         */
        System.out.println("--------rint-----------");
        System.out.println(Math.rint(10.1)); // 10.0
        System.out.println(Math.rint(10.7)); // 11.0
        System.out.println(Math.rint(11.5)); // 12.0
        System.out.println(Math.rint(10.5)); // 10.0
        System.out.println(Math.rint(10.51)); // 11.0
        System.out.println(Math.rint(-10.5)); // -10.0

        /**
         * round 四舍五入，float时返回int值，double时返回long值
         */
        System.out.println("--------round-----------");
        System.out.println(Math.round(10)); // 10
        System.out.println(Math.round(10.1)); // 10
        System.out.println(Math.round(10.7)); // 11
        System.out.println(Math.round(10.5)); // 11
        System.out.println(Math.round(10.51)); // 11
        System.out.println(Math.round(-10.5)); // -10
    }

    @Test
    /** 获取100以内的随机数！ */
    public void randomHundred(){
        // Math.random() 获取0-1 的随机数! 包含0!
        System.out.println("随机0-1");
        System.out.println(Math.random());

        System.out.println("随机0-100");
        System.out.println((int)(Math.random()*100)+1);
        /**
         * (int)(Math.random()*100)+1
         *  因为: Math.random() 获取0-1 的随机数! 包含0!
         *      *100 获取随机数进两位小数,强制转换(int) 会截取小数点..
         *      因为包含0 所以+1 就不会出现呢0 的情况了！！！
         */
    }


    /**
     * Java BigDecimal
     */
    @Test
    public void bigDecimal(){
        System.out.println(0.1+0.2);
        System.out.println(0.3-0.1);
        System.out.println(0.2*0.1);
        System.out.println(0.3/0.1);
    }
}


