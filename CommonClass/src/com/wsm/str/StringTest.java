package com.wsm.str;

/* String类学习: */
public class StringTest {
    public static void main(String[] args) {
        //字面量形式赋值:
        String a = "wsm";
        String b = "wsm";
        System.out.println(a==b);       //返回true

        //new String(); 赋值
        String w1 = new String("WSM");
        String w2 = new String("WSM");
        System.out.println(w1==w2);     //返回false
        /**因为
         *  Java 每new一次都会在堆空间开辟出一个新的空间,栈空间也开辟出新的空间 (栈空间 指向 堆空间的地址...)
         */
    }
}
