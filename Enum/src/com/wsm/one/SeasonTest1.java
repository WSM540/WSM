package com.wsm.one;

/**
 * 自定义枚举类学习 Season季节: 一年有四个季节是固定是刚好用来做枚举类
 */
public class SeasonTest1 {
    public static void main(String[] args) {
        //春天
        Season chu = Season.SPRING;
        System.out.println(chu.toString());
        /**
         * ok,这就是自定义枚举类, 类的对象是有固定个数的！且不可更改, 救像一种规则固定的对象...
         *      常用于规则状态的定义: 商品: 缺货 代发货 待付款...等固定的状态;
         * */
    }
}

class Season{
    //1.私有化类的构造器,并给对象属性赋值: 如果有对象属性的话顺便给属性赋值~
    private Season(String seasonName,String seasonDesc){
        this.seasonName = seasonName;
        this.seasonDesc = seasonDesc;
    }

    //2.声明Season对象的属性:private final修饰,因为枚举对象是固定的不能更改的.
    private final String seasonName;
    private final String seasonDesc;

    //3.创建当前类的 固定数量的对象(一年四季):  public static final的
        //因为是私有的构造函数,类中进行创建... 如果枚举类只有一个对象,则就是一种单例模式! `饿汉式`
    public static final Season SPRING = new Season("春天","春暖花开");
    public static final Season SUMMER = new Season("夏天","夏日炎炎");
    public static final Season AUTUMN = new Season("秋天","秋高气爽");
    public static final Season WINTER = new Season("冬天","冰天雪地");

    //ok,到这儿,自定义的枚举类就创建好了
    //如果还有其它需求可以进行扩展... 创建方法:

    /** 获取枚举类对象的属性 */
    /** 常量只有get(); 方法 */
    public String getSeasonName() {
        return seasonName;
    }
    public String getSeasonDesc() {
        return seasonDesc;
    }

    /** toString()方法 */
    @Override
    public String toString() {
        return "Season{" +
                "seasonName='" + seasonName + '\'' +
                ", seasonDesc='" + seasonDesc + '\'' +
                '}';
    }
}