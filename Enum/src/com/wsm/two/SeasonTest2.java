package com.wsm.two;

import javax.sound.midi.Soundbank;

/***
 * JDK1.5 新增enum
 */
public class SeasonTest2 {
    public static void main(String[] args) {
        /** 秋天 */
        Season AUTUMN = Season.AUTUMN;
        System.out.println(AUTUMN);         //尝试注释 toString()方法输出: AUTUMN (对象名)
        //enum extends Java.lang.enum类

        /** enum常用方法 */
        //values()方法：
        //  返回枚举类型的对象数组。该方法可以很方便地遍历所有的枚举值
        //valueOf(String str)
        //  可以把一个字符串转为对应的枚举类对象。要求字符串必须是枚举类对象的“名字”。如不是，会有运行时异常：IllegalArgumentException。
        //toString()            返回当前枚举类对象常量的名称
        /** values() */
        System.out.println("enum常用方法values()");
        Season[] values = Season.values();              //返回该enum 的所有对象
        //遍历结果集
        for (Season value : values) {
            System.out.println(value);
        }

        /** valueOf(String str) */
        System.out.println("enum常用方法valueOf(String str)");
        Season winter = Season.valueOf("WINTER");       //根据固定对象名,返回对应的enum
        System.out.println(winter);

        /** switch用法
         *      判断对象属于那一个...
         * */
        System.out.println("enum switch用法");
        switch (winter){
            case SPRING:
                System.out.println("春");
             break;
            case SUMMER:
                System.out.println("夏");
                break;
            case AUTUMN:
                System.out.println("秋");
                break;
            case WINTER:
                System.out.println("冬");
                break;
            default:
                System.out.println("没有匹配！");
        }

    }
}

/** 可以于自定义枚举进行比较查看~ */
enum Season{
    //1.必须在枚举类的第一行声明枚举类对象
    //枚举类的所有实例必须在枚举类中显式列出(, 分隔 ; 结尾)
    //因为枚举,本机不可修改由类访问可以取消: public static final 类
//    public static final Season SPRING = new Season("春天","春暖花开");
    SPRING("春天","春暖花开"),
    SUMMER("夏天","夏日炎炎"),
    AUTUMN("秋天","秋高气爽"),
    WINTER("冬天","冰天雪地");

    //2.声明Season对象的属性:private final修饰
    private final String seasonName;
    private final String seasonDesc;

    //3.私有化类的构造器,并给对象属性赋值
    private Season(String seasonName,String seasonDesc){
        this.seasonName = seasonName;
        this.seasonDesc = seasonDesc;
    }

    //扩展方法
    /** get */
    public String getSeasonName() {
        return seasonName;
    }

    public String getSeasonDesc() {
        return seasonDesc;
    }

    /** toString
     *      注释方法直接输出发现,即使不重新toString(); 也不是打印对象的: 地址值(栈指向堆的地址！)
     *      因为: enum类,是继承于 Java.lang.enum的,而不是 Object类
     * */
//    @Override
//    public String toString() {
//        return "Season{" +
//                "seasonName='" + seasonName + '\'' +
//                ", seasonDesc='" + seasonDesc + '\'' +
//                '}';
//    }
}