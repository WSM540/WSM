package com.wsm.three;

public class SeasonTest3 {
    public static void main(String[] args) {
        Season autumn = Season.AUTUMN;
        autumn.show();
    }
}

//使用enum关键字枚举类 implements 实现接口,重写show()方法~
enum Season implements Info{
    SPRING("春天","春暖花开"),
    SUMMER("夏天","夏日炎炎"),
    AUTUMN("秋天","秋高气爽"),
    WINTER("冬天","冰天雪地");

    //2.声明Season对象的属性:private final修饰
    private final String seasonName;
    private final String seasonDesc;

    //2.私有化类的构造器,并给对象属性赋值

    private Season(String seasonName,String seasonDesc){
        this.seasonName = seasonName;
        this.seasonDesc = seasonDesc;
    }

    @Override
    public void show() {
        System.out.println("这是一个季节~");
    }
}

/** 接口 */
interface Info{
    void show();
}