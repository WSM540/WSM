package com.wsm.comparatest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class ComparaTest {
    //Java String、包装类等实现了Comparable接口
    public static void main(String[] args) {
        //String 类实现了, Comparable 接口！并且实现Comparable 接口！可以直接使用: Arrays.sort  Collections.sort  进行排序!
        String[] arr = new String[]{"AA","cc","ac","dd","aa","FF","ff"};
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr)); //[AA, FF, aa, ac, cc, dd, ff]

        //Char 的包装类, Character 实现Comparable 接口！按照字符的Unicode值来进行比较;
        char a = 'a';
        char b = 'b';
        System.out.println("a="+(int)a+"\n"+"b="+(int)b);
        Character aC = 'a';
        Character bC = 'b';
        System.out.println(aC.compareTo(bC));               // a<b = -1

        //Boolean 包装类实现了, Comparable 接口！
        Boolean ok = true;
        Boolean no = false;
        System.out.println("Boolean类型: true > false:"+ok.compareTo(no));

        //int 等基本数据类型的包装类实现了, Comparable 接口！
        Integer one = 1;
        Integer two = 2;
        System.out.println("基本数据类型包装类比较,直接根据值进行比较:"+one.compareTo(two));

        //Data 日期类型实现, Comparable 接口！
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        Date thisYear = null;
        Date lastYear = null;
        try {
            thisYear = sdf.parse("2021");
            lastYear = sdf.parse("2020");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("日期类型比较,最近的日期最大:"+thisYear.compareTo(lastYear));
    }
}
