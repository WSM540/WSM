package com.wsm.comparable;
import java.util.Arrays;
/** 测试类 */
public class CommodityTest {
    public static void main(String[] args) {
        Commodity[] commodities = new Commodity[5];
        commodities[0] = new Commodity("a",1,1,1);
        commodities[1] = new Commodity("b", 1,1,1 );
        commodities[2] = new Commodity("c", 2,1,1 );
        commodities[3] = new Commodity("d", 3,1,1 );
        commodities[4] = new Commodity("a", 4,1,1 );
        System.out.println("第一次遍历数组:");
        for (Commodity commodity : commodities) {
            System.out.println(commodity);
        }
        //调用方法,给数组排序...
        Arrays.sort(commodities);
        System.out.println("Arrays.sort排序后遍历数组:");
        //name 从大到小~
        System.out.println("a="+(int)'a');
        System.out.println("b="+(int)'b');
        for (Commodity commodity : commodities) {
            System.out.println(commodity);
        }
    }
}
