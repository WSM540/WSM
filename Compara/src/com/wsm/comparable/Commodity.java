package com.wsm.comparable;
/**
 * 创建一个商品类；
 */
public class Commodity implements Comparable {
    //商品名
    private String name;
    //商品价格
    private int price;
    //商品销量
    private int sales;
    //点赞数
    private int like;
    //...更多不举例子了...

    /** 实现implements Comparable接口,重写 compareTo(Object o)类 */
        /** 指明商品比较大小的方式:按照价格从低到高排序,再按照产品名称从高到低排序 */
    @Override
    public int compareTo(Object o) {
        // instanceof: 判断传入参数类型是否是 商品类型,进入if 进行比较！
        if(o instanceof Commodity){
            // Object 类型强制转换,父类向下转换！
            Commodity o1 = (Commodity) o;

            if(this.price > o1.price){              //如果当前对象 大于> 传入对象返回 1;
                return 1;
            }else if(this.price < o1.price){        //如果当前对象 小于< 传入对象返回 -1;
                return -1;
            }else {                                 //相等则在进行判断...移此类推返回 0;
                //return 0;                         //相等,应该返回0 但我们还需要进行判断商品名称从高到底~
                //因为商品名称是 String类型, String 本身已经实现了 comparable 所以👇👇
                return -this.name.compareTo(o1.name);//默认都是从小到大排序,加个 - 号取反~
            }
        }
        throw new RuntimeException("传入的数据类型不一致！");
    }

    //有参构造
    public Commodity(String name, int price, int sales, int like) {
        this.name = name;
        this.price = price;
        this.sales = sales;
        this.like = like;
    }

    //重写toString 不然打印地址值！
    @Override
    public String toString() {
        return "Commodity{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", sales=" + sales +
                ", like=" + like +
                '}';
    }

    //省略 get/set..
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getSales() {
        return sales;
    }

    public void setSales(int sales) {
        this.sales = sales;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }


}
