package com.wsm.packclass;
/** 包装类Demo */
public class PackClass {
    public static void main(String[] args) {
        //Integer 是int 类型的包装类, 内置了大量的方法操作
        System.out.println("int 的最大值:"+Integer.MAX_VALUE);
        System.out.println("int 的最小值:"+Integer.MIN_VALUE);
        //...

        //构造方法:
        //  new Integer(int);       new Integer(String);
        //  将int 和 String　转换成Integer对象; (JDK8已过时  ~)


        //创建一个Integer
        //1.构造函数
        Integer c1 = new Integer("540");
        //2.自动装箱
        Integer c2 = 540;
        //3.使用提供的 valueof( int/String );
        // 构造函数方式已经被淘汰了... 使用String 注意String必须满足是数字符, 不可以是 abc...
        Integer c3 = Integer.valueOf("540");
        //因为 Integer 是对象了,每一个对象都会又一个对应栈地址, == 比较地址所以返回false
        System.out.println(c1==c2);
        System.out.println(c1==c3);
        System.out.println(c2==c3);
        //即使这样也不例外！
        Integer c22 = 540;
        System.out.println(c2==c22);
        //但 Integer 和 int 进行比较是直接比较值, 底层会进行 "自动拆箱"
        int c33 = 540;
        System.out.println(c3==c33);
        //Integer 和 int 可以进行,数值计算/比较~
        System.out.println(c3+c33);


        //自动装箱 拆箱
        Integer w = 123;
        System.out.println("自动装箱:"+w);

        int w1 = new Integer("123");
        System.out.println("自动拆箱:"+w1);
        System.out.println("自动装箱拆箱就是: 基本数据类型 和 引用类型(包装类), 之间的相互转换~");


        //String 包装类 基本数据类型  相互转换
        /**  基本类型转换为字符串(3) */
        int c = 10;
        //使用包装类的 toString() 方法
        String str1 = Integer.toString(c);
        //使用String类的 valueOf() 方法
        String str2 = String.valueOf(c);
        //用一个空字符串加上基本类型，得到的就是基本类型数据对应的字符串
        String str3 = c + "";

        /**  字符串转换为基本类型 */
        String str = "8";
        //调用包装类的parseXxx()静态方法
        int d = Integer.parseInt(str);
        //调用包装类的valueOf()方法转换为基本类型的包装类，会自动拆箱
        int e = Integer.valueOf(str);

        /**  字符串转换为包装类 */
        //通过构造函数 字符参数
        //通过valueof("");
        Integer integer = Integer.valueOf("8");
        System.out.println(integer);
    }
}
