package com.wsm.comparator;

import java.util.Arrays;
import java.util.Comparator;

/** ComparatorTest测试Demo */
public class ComparatorTest {
    //直接拿String　类进行操作, String类本身实现了 comparable接口;
    public static void main(String[] args) {
        /** 混乱的String[] */
        String[] arr = new String[]{"CC","MM","KK","AA","DD","GG","JJ","EE"};
        //默认从小到大排序
        System.out.println("默认从小到大排序");
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));

        System.out.println("按照字符串从大到小的顺序排列");
        //按照字符串从大到小的顺序排列, new Comparator(){ ... } 内部类操作;
         Arrays.sort(arr,new Comparator(){
             //重写compare(Object o1, Object o2) 方法
             @Override
             public int compare(Object o1, Object o2) {
                 //类型判断全栈类型转换
                 if(o1 instanceof String && o2 instanceof  String){
                     String s1 = (String) o1;
                     String s2 = (String) o2;
                     return -s1.compareTo(s2);
                 }
                 throw new RuntimeException("输入的数据类型不一致");
             }
        });
        System.out.println(Arrays.toString(arr));
    }
}
