package com.wsm.dly;

/**
 * 如何自定义异常类？
 *      1. 继承于现有的异常结构：RuntimeException运行时异常不必须处理 、Exception 编译时异常必须处理的异常！
 *      2. 提供全局常量：serialVersionUID
 *      3. 提供重载的构造器
 */

//IDEA中Java类图标为闪电符号说明：这是一个Exception类的子类
public class MyException extends Exception{
    //序列化唯一ID
    private static final long serialVersionUID = -7034897193246939L; 	//唯一序列号, 表示这个类的唯一标识！

    //无参构造！
    public MyException(){
    }

    //有参构造！
    public MyException(String msg){
        //给父类的,异常信息赋值！
        super(msg);
    }
}
