package com.wsm.dly;

public class StudentTest {

    public static void main(String[] args) {
        try {
            Student s = new Student();
            s.regist(-1001);
            System.out.println(s);
        } catch (Exception e) {
            //打印堆栈和异常信息！
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
}

//学生类
class Student{

    private int id;
    //判断如果为负数,手动抛出异常！
    public void regist(int id) throws Exception {   //throws抛出异常,给调用者处理！
        if(id > 0){
            this.id = id;
        }else{
//			System.out.println("您输入的数据非法！");
            //手动抛出异常对象
//			throw new RuntimeException("您输入的数据非法！");
//			throw new Exception("您输入的数据非法!");
            throw new MyException("不能输入负数");

            //错误的
//			throw new String("不能输入负数");         //throw 必须抛出异常类 或 子类..
        }

    }

    @Override
    public String toString() {
        return "Student [id=" + id + "]";
    }
}