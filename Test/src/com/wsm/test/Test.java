package com.wsm.test;

import javax.sound.midi.Soundbank;
import javax.xml.transform.Source;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class Test {

    public static void main(String[] args) {
        System.out.println("Java遵循‘负负得正’ 数学公式:"+(-(-1)));
    }

    @org.junit.Test
    public void show(){
//        long l =20210909;
//        l = l/100;
//        System.out.println(l);

        String str = "2020-09-09";
        //指定转换格式
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(str, fmt);
        long[] ls = new long[12];
        for(int i = 0;i <12; i++){
            LocalDate localDate = date.minusMonths(i+1);
            String ss = localDate.toString().substring(0,7);
            long ll = Long.valueOf(ss.replaceAll("-",""));
            ls[i]  = ll;
        }

        for (long l : ls) {
            System.out.println(l);
        }
    }



    /** 方案一 */
    @org.junit.Test
    public  void distinct() {
        List<String> lists = Arrays.asList(new String[]{"AA", "BB", "CC","CC"});
        HashSet<String> sets = new HashSet<>();
        for (String s : lists) {
            sets.add(s);
        }
        System.out.println("去重后的Set集合: "+sets);
    }

    @org.junit.Test
    /** 方案二 */
    public  void distinct2() {
        List<String> lists = Arrays.asList(new String[]{"AA", "BB", "CC","CC"});
        //方式一
        List<String> listNew = new ArrayList<String>(new TreeSet<String>(lists));
        System.out.println("TreeSet"+listNew);
        //方式二
        List<String> listNew2 = new ArrayList<String>(new LinkedHashSet<String>(lists));
        System.out.println("LinkedHashSet"+listNew);
    }

    @org.junit.Test
    /** 方案四 */
    public  void distinct4() {
        List<String> lists = Arrays.asList(new String[]{"AA", "BB", "CC","CC"});
        List<String> myList = lists.stream().distinct().collect(Collectors.toList());
        System.out.println("JDK8.0新特性:"+myList);
    }

}

