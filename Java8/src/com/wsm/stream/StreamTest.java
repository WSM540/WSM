package com.wsm.stream;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/** 强大的Stream Api */
public class StreamTest {
/** 创建 Stream */
    @Test
    public void test1(){
        /** 方式一: 通过集合: 集合.stream/parallelStream(); 返回一个stream顺序流/并行流~ */
        List<Emp> emps = Emp.getEmployees();

        //default Stream<E> stream() : 返回一个顺序流
        Stream<Emp> stream = emps.stream();
        //default Stream<E> parallelStream() : 返回一个并行流
        Stream<Emp> parallelStream = emps.parallelStream();
        /** Java8 Collection接口添加了新的方法 stream()、parallelStream()、forEach()和removeIf()... */

        /** 方式二: 通过数组: Arrays.stream(数组); 返回对应的Stream流对象！ */
        int[] arr = new int[]{1,2,3,4,5,6};
        //调用Arrays类的static <T> Stream<T> stream(T[] array): 返回一个流
        IntStream intstream = Arrays.stream(arr);              /** 基本数据类型返回,对应的 xxxstream  Stream流对象~ */
        Emp e1 = new Emp(1001,"Tom",34, 6000.38);
        Emp e2 = new Emp(1002,"Jerry",34, 6000.38);
        Emp[] arr1 = new Emp[]{e1,e2};
        Stream<Emp> empStream = Arrays.stream(arr1);                /** 自定类型数组,返回 Stream<自定义类型> 的Sream类型对象！ */

        /** 方式三: 通过Stream的of() */
        Stream<Integer> stream3 = Stream.of(1, 2, 3, 4, 5, 6);      /** 用的少了解即可~ */
    }

    /** 方式四: 创建无限流: 通过Stream类的静态方法 iterate()迭代 generate()生成 */
    public void test2(){
        /** 两种方式: 迭代流 生成流 */
//      迭代
//      public static<T> Stream<T> iterate(final T seed, final UnaryOperator<T> f)
        //参数 T , UnaryOperator函数式接口(内部方法: 传入参数T类型,返回T类型结果,正好用于对T的每次迭代的改变~)

        //Stream.iterate(0, t -> t + 2).forEach(System.out::println);
        //  注释原因: 迭代流会无限的迭代下去 +2 +2 +2...
        //  .limit(10) 之取前十个数据
        //  .forEach() 结束操作! 结束时候做的事情~

        //遍历前10个偶数
        Stream.iterate(0, t -> t + 2).limit(10).forEach(System.out::println);

//      生成
//      public static<T> Stream<T> generate(Supplier<T> s)
        //参数 Supplier函数式接口对象, 内部方法, 返回一个T类型对象... 可以根据后一个规则无限的来生成一些数据~
        Stream.generate(Math::random).limit(10).forEach(System.out::println);       /** 生成10个小于 1 随机数！ */
    }
}
