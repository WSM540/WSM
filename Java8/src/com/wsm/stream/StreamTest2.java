package com.wsm.stream;

import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/** Stream中间操作
 *      创建完Stream流对象之后,就可以通过 流对象S.xx().xx().xx() 各种的中间操作,完成对 流种数据的计算: 筛选 切片 映射 排序...等操作
 *      中间操作, 是多个方法, 每个方法可以对流中的数据进行筛选计算~
 *      多个方法可以像链条一样 拼接操作~
 * */
public class StreamTest2 {
    /** 筛选与切片 */
    @Test
    public void test1(){
        System.out.println("筛选与切片");
        //获取 emp 集合~
        List<Emp> employees = Emp.getEmployees();
        //JDK8 Collection接口新增,foreach(); 方法: 遍历结果集操作~
        employees.forEach(System.out::println);

        //集合创建Stream 流~
        Stream<Emp> stream = employees.stream();
        System.out.println("\nfilter: 从流中排除某些元素");
        System.out.println("练习: 查询员工表中薪资大于7000的员工信息");
        stream.filter(e->e.getSalary()>7000).forEach(System.out::println);  //filter(Predicate<T>); lambda表达式,每次传入一个流中对象,返回一共 Boolean结果,过滤掉false数据！

        /** 注意一共Stream 使用之后就不可以在次使用,流已经关闭了... 但创建流的,集合依然存在~ */
//        stream.filter(e->e.getSalary()>7000).forEach(System.out::println);  //异常：stream has already been operated upon or closed 需要重新创建一共新的流~

        System.out.println("\ndistinct：筛选，对流中元素进行 hashCode() 和 equals() 去除重复元素");
        employees.stream().distinct().forEach(System.out::println);

        System.out.println("\nlimit(n)——截断流，使其元素不超过给定数量(获取前几个元素~)");
        employees.stream().limit(3).forEach(System.out::println);

        System.out.println("\nskip(n) —— 跳过元素(跳过前几个元素不要~)");
        employees.stream().skip(3).forEach(System.out::println);
    }

    /** **映 射** */
    @Test
    public void test2(){
        System.out.println("映射");
        System.out.println("\nmap(Function f): ——接收一个函数作为参数，将元素转换成其他形式或提取信息，该函数会被应用到每个元素上，并将其映射成一个新的元素");
        List<String> strlist = Arrays.asList("aa", "bb", "cc", "dd");
        strlist.stream().map(str -> str.toUpperCase()).forEach(System.out::println);   //遍历每一个元素,对元素进行二次操作~

        System.out.println("\n过年了,给所有员工工资+1000");
        //获取 emp 集合~
        List<Emp> employees = Emp.getEmployees();
        System.out.println("遍历一遍集合:");
        employees.stream().forEach(System.out::println);
        System.out.println("\n加薪: map( 内部函数式接口方法,要求传入什么参数类型,则返回什么类型~ );　顺便去重 distinct()");
        employees.stream().map(emp -> {emp.setSalary(emp.getSalary()+1000.0); return emp; }).distinct().forEach(System.out::println);

        System.out.println("\nflatMap(f); 是map(f)的高级版~");
        System.out.println("1.Map(f) 实现遍历每一个strlist 的字符");
        Stream<Stream<Character>> streamStream = strlist.stream().map(StreamTest2::fromStringToStream);
        streamStream.forEach(s ->{
            s.forEach(System.out::println);
        });
        System.out.println("2.flatMap(f) 实现遍历每一个strlist 的字符");
        Stream<Character> characterStream = strlist.stream().flatMap(StreamTest2::fromStringToStream);
        characterStream.forEach(System.out::println);
        System.out.println("flatMap 会将,内部的每一个元素进行操作,如果是Stream元素也会重新拆开执行~");

        System.out.println("\n下面的映射,就不详细介绍了: mapToInt(T) mapToLong(T) mapToDouble(T) 传入泛型T 返回对应的类似数据~");
    }

    /** 将字符串中的多个字符构成的集合转换为对应的Stream的实例 */
    public static Stream<Character> fromStringToStream(String str){
        ArrayList<Character> list = new ArrayList<>();
        for(Character c : str.toCharArray()){
            list.add(c);
        }
        return list.stream();
    }

    /** flatMap 和 Map :就类似于,数组里面套数组 集合里面套集合 遍历数组和集合的所有元素~*/
    @Test
    public void flat(){
        ArrayList list1 = new ArrayList();
        list1.add(1);
        list1.add(2);
        list1.add(3);

        ArrayList list2 = new ArrayList();
        list2.add(4);
        list2.add(5);
        list2.add(6);
        /** map 就相当于是 add(集合) */
//        list1.add(list2);
        /** flatMap 就相当于是 addAll(集合) 将集合拆分,对每个单独的元素进行操作~*/
//        list1.addAll(list2);
        System.out.println(list1);
    }


    /** 排序 */
    @Test
    public void test3(){
        System.out.println("sorted()——自然排序");
        System.out.println("注意,如果是自定义类型需要,实现Comparable接口,int 默认实现了Comparable");
        List<Integer> list = Arrays.asList(12, 43, 65, 34, 87, 0, -98, 7);
        list.stream().sorted().forEach(System.out::println);

        System.out.println("sorted(Comparator com)——定制排序");
        list.stream().sorted((e1,e2)->-e1.compareTo(e2)).forEach(System.out::println);
        /**
         * 排序 A 比较 B
         *  返回 0  A相等B
         *  返回 -1 A小于B
         *  返回 1  A大于B
         * */
    }
}
