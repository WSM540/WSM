package com.wsm.outher;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
/** Base64 加解密 */
public class Base {
    public static void main(String[] args) {
        final String text = "Java慈祥,yyds!";

        final String encoded = Base64.getEncoder().encodeToString( text.getBytes( StandardCharsets.UTF_8 ) );
        System.out.println("加密:"+encoded);

        final String decoded = new String(Base64.getDecoder().decode( encoded ),StandardCharsets.UTF_8 );
        System.out.println("解密:"+decoded);
    }
}
