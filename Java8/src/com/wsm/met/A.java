package com.wsm.met;

/** 自定义函数式: */
@FunctionalInterface
public interface A {
    //上面实例是一个 无参, 这里定义一个有参的方法();
    public void af(int i);
}
