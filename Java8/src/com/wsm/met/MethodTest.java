package com.wsm.met;

import org.junit.Test;

import java.util.Comparator;

/** 方法引用测试类 */
public class MethodTest {

/** JDK1.8之前创建A接口实例： */
    @Test
    public void Test(){
        System.out.println("Test");

        A a = new A() {
            @Override
            public void af(int i) {
                System.out.println("JDK8之前实现接口~ 参数列表i="+i);
            }
        };

        a.af(1);
    }

/** Lambda表达式创建A接口实例： */
    @Test
    public void Test2(){
        System.out.println("Test2");
        //Lambda表示式实现A接口:
        A a =  i ->  System.out.println("Lambda表达式实现接口 参数列表i="+i);

        int i = 2;
        a.af(i);
    }

/** 对象 :: 非静态方法 */
    /** 升级Lambda表达式: 方法引用 */
    @Test
    public void Test3(){
        System.out.println("Test3");
    //① 创建B  类对象;
        B b = new B();
    //② 方法引用:
        A a = b::bf;
    //③ 调用方法~
        a.af(3);
        /** 因为 af(i) 和 bf(i) 方法实现`af实现的操作bf已经完成了` 返回值 参数列表相同~  */
        /** 则满足方法引用,直接使用！ */
    }

/** 类 :: 静态方法 */
    @Test
    public void Test4(){
        System.out.println("Test4");
     //① 方法引用:
        A a = B::bsf;           //直接通过 类::匹配的静态方法()~
     //② 调用方法~
        a.af(4);
    }

/** 类 :: 实例方法  (有难度) */
    /** 以Comparator 方法举例: int comapre(T t1,T t2) 方法 */
    /** String中的int t1.compareTo(t2) */
    @Test
    public void Test5(){
        System.out.println("Test5");
        /** lambda表达式实现 */
        Comparator<String> com1 = (s1, s2) -> s1.compareTo(s2);
        System.out.println("lambda比较两个字符串大小~"+com1.compare("abc","abd"));

        Comparator<String> com2 = String :: compareTo;
        System.out.println("方法引用: 类 :: 实例方法 比较两个字符串大小~"+com2.compare("abc","abd"));

        /**
         * 如果:
         *  A 函数式接口的的实现来源于~
         *    af(T1,T2);   T1类.方法(T2); 实现,则属于 类::实例方法;  的方法引用~
         * */
    }
}
