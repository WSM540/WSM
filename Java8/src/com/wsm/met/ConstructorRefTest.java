package com.wsm.met;

import org.junit.Test;

import java.util.Arrays;
import java.util.function.Function;
import java.util.function.Supplier;

/** 构造器/数组引用 */
public class ConstructorRefTest {
/** 构造器引用 */
    //JDK8提供的函数式接口 Supplier中的T get()  返回一个 T 类型对象
    //B 类的空参构造器: B()
    @Test
    public void Test(){
        /** JDK8之前 */
        Supplier<B> supB = new Supplier<B>() {
            @Override
            public B get() {
              return new B();
            };
        };
        System.out.println("JDK8之前");
        System.out.println("无参默认创建的对象: supB.get().getName();"+supB.get().getName());

        /** Lambda构造器引用 */
        Supplier<B>  supB1 = () -> new B();
        System.out.println("Lambda创建的对象: supB.get().getName();"+supB1.get().getName());

        Supplier<B>  supB2 = B :: new;
        System.out.println("构造器引用创建的对象: supB.get().getName();"+supB2.get().getName());
    }
/** 构造器引用2.0 */
    //Function中的R apply(T t);   Function<T, R>函数式接口,对类型为T的对象应用操作，并返回是R类型的对象
    @Test
    public void Test2(){
        System.out.println("Lambda创建的对象:");
        Function<String ,B> func1 = name -> new B(name);
        System.out.println("Lambda创建的对象: supB.get().getName()= "+func1.apply("wsm1").getName());

        System.out.println("*******\n");

        System.out.println("构造器引用创建的对象:");
        Function<String ,B> func2 = B :: new;           //参数自动匹配~
        System.out.println("Lambda创建的对象: supB.get().getName()= "+func2.apply("wsm2").getName());
    }

/** 数组引用 */
    @Test
    public void Test3(){
        Function<Integer,String[]> func1 = length -> new String[length];
        String[] arr1 = func1.apply(5);
        System.out.println(Arrays.toString(arr1));

        System.out.println("*******************");

        Function<Integer,String[]> func2 = String[] :: new;
        String[] arr2 = func2.apply(10);
        System.out.println(Arrays.toString(arr2));
    }
}
