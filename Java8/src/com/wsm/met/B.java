package com.wsm.met;

/** 自定义类,实现函数式接口 */
public class B {
    //方法参数列表,与函数式接口相同~
    public void bf(int i){
        System.out.println("对象::实例方法引用 参数列表i="+i);
    }

    //类的静态方法, bsf(int i);
    public static void bsf(int i){
        System.out.println("类::static静态方法引用 参数列表i="+i);
    }

    public String name = "default";

    public B(){
        System.out.println("B() 无参构造");
    }

    public B(String name) {
        System.out.println("B(name,age) 有参构造");
        this.name = name;
    }
    //省略get/set
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
