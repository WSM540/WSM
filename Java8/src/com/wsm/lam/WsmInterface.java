package com.wsm.lam;
/** 自定义函数式接口 */

/** 注解可以省略,没有影响,注解只是对程序编写的一个提醒标识~
 *  提示你: 这是一个函数式接口！ */
@FunctionalInterface
public interface WsmInterface {
    public abstract void show();
}
