package com.wsm.lam;

import org.junit.Test;

import java.util.Comparator;

/**
 * Lambda表达式的使用举例
 */
public class LambdaTest {

    @Test
    public void test1(){
        /** JDK8之前,定义Runnable 接口实现多线程.*/
        //1.  类——>实现Runnable接口,创建实例...填入Thread(Runnable r); .start(); 启动线程
        //2.  匿名内部类方式,获取Runnable 接口实例: 创建一个接口实例
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("匿名内部类实现Runnable 接口实例");
            }
        };
        Thread thread1 = new Thread(runnable);
        thread1.start();


        /**JDK8之后,定义Runnable Lambda接口实现多线程.*/
        //Lambda
        //  ->左侧: 指定了 Lambda 表达式需要的参数列表, 这里参数列表是空
        //  ->右侧: 指定了 Lambda 体,是抽象方法的实现逻辑,也即Lambda 表达式要执行的功能;
        Runnable runnable2 = () -> { System.out.println("Lambda实现Runnable 接口实例");};
        Thread thread2 = new Thread(runnable2);
        thread2.start();


        //Lambda 优化:
        //  lambda形参列表的参数类型可以省略(类型推断), 如果lambda形参列表只有一个参数,  其一对()也可以省略
        //  lambda体应该使用一对{}包裹, 如果lambda体只有一条执行语句,可能是return语句, 可以省略这一对{}和return关键字.
        Runnable runnable3 = () -> System.out.println("Lambda优化实现Runnable 接口实例");
        Thread thread3 = new Thread(runnable3);
        thread3.start();

        System.out.println();
        System.out.println("创建的 Runnable接口实例,正常使用！");
    }

    @Test
    public void test2(){
        /** 练习: Comparator 定制排序
         *      Comparator接口,也是一个 "函数式接口": 只包含一个抽象方法的接口，称为函数式接口
         */
        /** JDK8之前,定义Runnable 接口实现多线程.*/
        System.out.println("自然排序/定制排序: 比较基本/引用数据类型,A>B=1 A<B=-1 A==B=0");
        Comparator<Integer> com1 = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return Integer.compare(o1,o2);
            }
        };
        System.out.println("定制排序1: "+com1.compare(1, 2));


        /**JDK8之后,定义Runnable Lambda接口实现多线程.*/
        //Lambda
        Comparator<Integer> com2 = (Integer o1, Integer o2) -> { return Integer.compare(o1,o2); };
        System.out.println("定制排序2: "+com2.compare(3, 2));

        //Lambda 优化:
        //类型推断: 省略类型
        //如果lambda体只有一条执行语句,可能是return语句, 可以省略这一对{}和return关键字.
        Comparator<Integer> com3 = (o1,o2) ->  Integer.compare(o1,o2);
        System.out.println("定制排序3: "+com3.compare(4, 3));

        /** 方法引用 */
        Comparator<Integer> com4 = Integer :: compare;      //后面介绍
        System.out.println("定制排序4: "+com4.compare(2, 2));
    }
}