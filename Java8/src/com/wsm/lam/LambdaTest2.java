package com.wsm.lam;

import org.junit.Test;

import java.util.function.Consumer;

/** 四大核心函数式接口Function */
public class LambdaTest2 {
/** 正常情况下,函数式接口 实例,当作方法参数传递在方法中完成事情~ */

/** 消费型接口 Consumer<T>     void accept(T t) */
    /** 声明一个方法传入Consumer<T> 对象实例:  */
    public void con(Double money, Consumer<Double> con){        //<T> 泛型规范了传入的类型~ Double~
        System.out.println("con方法调用~");
        con.accept(money);
        System.out.println("");
    };

    /** J8前 */
    @Test
    public void ConsumerTest(){
        //要传入的参数！
        Double dd = 540.0;
        System.out.println("调用 con(Double,Consumer<Double>) 方法");

        //方式一 创建函数式接口的对象,传入接口的实例: (创建方式,匿名内部类~
        Consumer<Double> con1 = new Consumer<Double>() {
            @Override
            public void accept(Double aDouble) {
                System.out.println("接口实例类,参数传递实现: 身上还有"+aDouble+"块钱!");
            }
        };
        //调用方法
        this.con(dd,con1);

        //方式二: 参数匿名内部类实现
        this.con(dd, new Consumer<Double>() {
            @Override
            public void accept(Double adouble) {
                System.out.println("匿名内部类实现: 身上还有"+adouble+"块钱!");
            }
        });
    }

    /** J8后 */
    @Test
    public void ConsumerTest2(){
        //要传入的参数！
        Double dd = 540.0;

        //JDK8 后Lambda表达式,对 参数匿名内部类 的升级
        /** 调用 con(Double,Consumer<Double>) 方法 */
        this.con(dd, (adouble) -> System.out.println("Lambda表达式实现: 身上还有"+adouble+"块钱!") );
    }

/** 供给型接口 Supplier<T>     T get() */
    //返回类型为 T 的对象;
    public void SupplierTest(){

    }

/** 函数型接口 Function<T,R>   R apply(T t) */


/** 断定型接口 Predicate<T>    boolean test(T t) */

}

