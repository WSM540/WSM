package com.wsm.annotation;

import java.lang.annotation.Annotation;

/** 步骤2 */
public class TestAnnotation {
    public static void main(String[] args) {
        //使用反射,来完成注解是实习,这里只是简单的输出.
        //  很多时候反射,扫描所有的类,并判断注解的参数 完善类添新的功能~
        Class clazz = Wsm.class;
        Annotation[] annotations = clazz.getAnnotations();      //获取当前类的注解集合~
        for(int i = 0;i < annotations.length;i++){              //输出注解集合的值！
            System.out.println(annotations[i]);
        }
    }
}

/** 步骤1 */
//@MyAnnotation                     //啥也不写默认值~
//@MyAnnotation(value = "abc")      //手动填写
@MyAnnotation("efg")              //如果注解的只有一个属性且属性名是 value: 可以省略不写
class Wsm{
    //一个空的类,使用 自定义注解~
    @MyAnnotation
    public void show(){
        System.out.println("show方法()");
    }
}