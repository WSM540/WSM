package com.wsm.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/** 自定义注解1.0 */
@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnnotation {

    //这是自定义注解中,属性的声明方式: 数据类型 参数名;
//    String value();

    //指定成员变量的初始值可使用 default 关键字
    String value() default "wsm";                   /** 如果只有一个参数成员，建议使用参数名为value */
}
