package com.wsm.annotation2;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnnotations {
    MyAnnotation[] value();             //注解属性,是可重复注解的数组~
}


/** 斯莱特林 */
// 🐍院传统: 有野心，精明，重视荣誉，审时度势，明哲保身，胜利至上
// 自研至上,外包都是 "泥巴种！"