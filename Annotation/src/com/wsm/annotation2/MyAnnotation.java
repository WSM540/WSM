package com.wsm.annotation2;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Repeatable(MyAnnotations.class)           //1.8新增可重复注解,知道注解容器~
public @interface MyAnnotation {
    String value();                         //参数~
}
