package com.wsm.thread;
//继承Thread类完成窗口买票!

/**
 * 例子：创建三个窗口卖票，总票数为100张.使用继承Thread类的方式
 * 存在线程的安全问题，待解决。
 */
class Window extends Thread{
    /* 使用 static修饰和不用static修饰 */
    //private int ticket = 100;
    private static int ticket = 100;
    /**static
     *      static 修饰的属性,属于类,该类的所有的对象都共享
     *      因为, 继承Thread 实现线程,没一次new 创建线程对象,内部属性不是static修饰, 每次都是一个新的对象100 这样三个窗口就相当于都有100个票
     *      而, 不是一共由100 张票的了！
     */
    @Override
    public void run() {
        while(true){
            if(ticket > 0){
                System.out.println(getName() + "：卖票，票号为：" + ticket);
                ticket--;
            }else{
                break;
            }
        }
    }
}

public class ThreadWindow {
    public static void main(String[] args) {
        Window t1 = new Window();
        Window t2 = new Window();
        Window t3 = new Window();

        t1.setName("窗口1");
        t2.setName("窗口2");
        t3.setName("窗口3");

        t1.start();
        t2.start();
        t3.start();
    }
}


