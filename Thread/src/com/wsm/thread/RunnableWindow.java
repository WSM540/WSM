package com.wsm.thread;
//实习Runnable接口,实现窗口买票！

/**
 * 例子：创建三个窗口卖票，总票数为100张.使用实现Runnable接口的方式
 * 存在线程的安全问题，待解决。
 */
class Window1 implements Runnable{

    //Runnable实现线程,内部数据可以共享！
    private int ticket = 100;
    @Override
    public void run() {
        while(true){
            if(ticket > 0){
                System.out.println(Thread.currentThread().getName() + ":卖票，票号为：" + ticket);
                ticket--;
            }else{
                break;
            }
        }
    }
}

public class RunnableWindow {
    public static void main(String[] args) {
        //创建Runnable 实现类
        Window1 w = new Window1();
        //根据w 创建三个线程,因为都是用的是通一个对象w 所以,内部的实例数据共享！ 不需要static修饰可以实现票共享！
        Thread t1 = new Thread(w);
        Thread t2 = new Thread(w);
        Thread t3 = new Thread(w);

        t1.setName("窗口1");
        t2.setName("窗口2");
        t3.setName("窗口3");

        t1.start();
        t2.start();
        t3.start();
    }
}
